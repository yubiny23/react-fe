import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button,Badge } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  


const BtEmpresasUsuarios = (props) => {

  const id = props.match.params.id;
  const [data, setData] = useState([])
  const [emp, setEmp] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      try {

        axios.get( (getUrlBE() + '/empresa/'+id)).then(response => {
            
          setEmp(response.data.empresa);
            
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
    fetchData()
    
   
  }, [])

  
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios.get( (getUrlBE() + '/empresasuall/'+id)).then(response => {
        setData(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
  
    // call the async fetchData function
    fetchData()
  
  }, [])


  return (
    <Page
      title="Usuarios de empresas"
      breadcrumbs={[{ name: 'Empresas', active: true }]}
      className="EmpresasTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Usuarios de empresa 
              <Badge color="primary">{emp}</Badge>

                <Link to={"/usuariosempresag/"+id} className="btn btn-primary float-right">Agregar</Link>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ ['striped']: true }}>
                        <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                            <th>Rol</th>
                            <th>Fecha creacion</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                          
                          
                          {data.map((u, index) => ( 

                            <tr>
                            <td scope="row">{u.nombres}</td>
                            <td >{u.apellidos}</td>
                            <td>{u.correo_electronico}</td>
                            <td>{u.id_empresa_rol}</td>
                            <td>{u.fecha_creacion}</td>
                            <td>
                              <Link to={"/editarempresau/"+u.id_empresa_usuario}  className="nav-link">
                                Editar
                              </Link>
                            <Link to={"/eliminarempresau/"+u.id_empresa_usuario} className="nav-link">
                                Eliminar
                              </Link>

                            </td>
                            </tr>

                          ))}

                            
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtEmpresasUsuarios;

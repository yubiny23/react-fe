import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button ,
Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar
} from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";



const BtEmpresaAgregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const id = props.match.params.id;
  
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Empresa', active: true }]}
      className="EmpresaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar administrador

              </CardHeader>
              
            <CardBody>
              <Form>
                <FormGroup row>
                  <Label for="lblEmpresa" sm={2}>
                    Empresa
                  </Label>
                  <Col sm={10}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese la empresa"
                      {...register("empresa", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblRS" sm={2}>
                    Raz&oacute;n Social
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese la raz&oacute;n social"
                      {...register("razon_social", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lbl-direccion" sm={2}>
                    Direcci&oacute;n
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese la direcci&oacute;n"
                      {...register("direccion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lbl-telefono_1" sm={2}>
                    Tel&eacute;fono
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese el tel&eacute;fono"
                      {...register("telefono_1", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lbl-codigo" sm={2}>
                    C&oacute;digo
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese el c&oacute;digo de la empresa"
                      {...register("codigo_empresa", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lbl-giro" sm={2}>
                    Giro
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese el giro"
                      {...register("giro", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary">Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary">Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtEmpresaAgregar;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  


const BtEmpresas = () => {

  const [data, setData] = useState([])
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios.get( (getUrlBE() + '/empresasall')).then(response => {
        setData(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
  
    // call the async fetchData function
    fetchData()
  
  }, [])


  return (
    <Page
      title="Empresas"
      breadcrumbs={[{ name: 'Empresas', active: true }]}
      className="EmpresasTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Listado de Empresas
                <Link to={"/agregarempresa"} className="btn btn-primary float-right">Agregar</Link>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ ['striped']: true }}>
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>C&oacute;digo</th>
                            <th>Raz&oacute;n social</th>
                            <th>Fecha creacion</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                          
                          
                          {data.map((empresa, index) => ( 

                            <tr>
                            <td scope="row">{empresa.empresa}</td>
                            <td >{empresa.codigo}</td>
                            <td>{empresa.razon_social}</td>
                            <td>{empresa.fecha_creacion}</td>
                            <td>
                              <Link to={"/usuariosempresa/"+empresa.id_empresa}  className="nav-link">
                                Usuarios
                              </Link>
                              <Link to={"/editarempresa/"+empresa.id_empresa}  className="nav-link">
                                Editar
                              </Link>
                            <Link to={"/eliminarempresa/"+empresa.id_empresa} className="nav-link">
                                Eliminar
                              </Link>

                            </td>
                            </tr>

                          ))}

                            
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtEmpresas;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  


const BtOfertaTrabajoa = () => {

  const [data, setData] = useState([])
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios.get( (getUrlBE() + '/ofertasall')).then(response => {
        setData(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
  
    // call the async fetchData function
    fetchData()
  
  }, [])


  return (
    <Page
      title="Plazas"
      breadcrumbs={[{ name: 'Plazas', active: true }]}
      className="PlazasTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Listado de Plazas
                <Link to={"/ofertaagregar"} className="btn btn-primary float-right">Agregar</Link>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ ['striped']: true }}>
                        <thead>
                        <tr>
                            <th>Oferta</th>
                            <th>Estado</th>
                            <th>Fecha creación</th>
                            <th>Creado por</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                          
                          
                          {data.map((cv, index) => ( 

                            <tr>
                            <td scope="row">{cv.oferta_trabajo}</td>
                            <td >{cv.estado_oferta}</td>
                            <td>{cv.fecha_creacion}</td>
                            <td>{cv.usuario}</td>
                            <td>
                              <Link to={"/ofertaver/"+cv.id_oferta_trabajo}  className="nav-link">
                                Ver
                              </Link>
                              
                            <Link to={"/ofertaeliminar/"+cv.id_curricid_oferta_trabajoulum} className="nav-link">
                                Eliminar
                              </Link>

                            </td>
                            </tr>

                          ))}

                            
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtOfertaTrabajoa;

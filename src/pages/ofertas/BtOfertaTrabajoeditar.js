import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import moment from "moment";

import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtOfertaTrabajoeditar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;
  const [oferta, setOferta] = useState([])
  
  
  //setFecha(new Date());

  useEffect(() => {

    //Experiencia laboral
    const fCurriculum = async () => {
      try {
        axios.get( (getUrlBE() + '/oferta/'+idCV)).then(response => {
        
        //console.log(f.dateFormat("Y"))
        //setFecha(response.data.datos.fecha_nacimiento)


         setValue("plaza",response.data.datos.oferta_trabajo)
         setValue("descripcion",response.data.datos.descripcion_puesto)
         setValue("perfil_academico",response.data.datos.perfil_academico)
         setValue("experiencia_laboral",response.data.datos.experiencia_laboral)
         
         setValue("salario_minimo",response.data.datos.salario_min)
         setValue("salario_maximo",response.data.datos.salario_max)


        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fCurriculum()
  }, [])
  

  const cCurriculum = () => {


    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('plaza', getValues('plaza') );
    bodyFormData.append('descripcion', getValues('descripcion'));
    bodyFormData.append('perfil_academico', getValues('perfil_academico'));
    bodyFormData.append('experiencia_laboral', getValues('experiencia_laboral'));
    bodyFormData.append('salario_minimo',   getValues('salario_minimo') );
    bodyFormData.append('salario_maximo', getValues('salario_maximo'));
    bodyFormData.append('id', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/ofertaeditar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro modificado con éxito")
            props.history.push('/ofertaver/'+response.data.datos.id)
        }else{

        }

    }).catch(error => {

    });
    
  }

  
  

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'Plaza', active: true }]}
      className="PlazaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Editar plaza

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblPlaza" sm={3}>
                    Plaza
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Escriba el nombre de la plaza"
                      {...register("plaza", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblDescripcion" sm={3}>
                    Descripción
                  </Label>
                  <Col sm={9}>
                  <input
                      type="textarea"
                      className="form-control"
                      placeholder="Descripción"
                      {...register("descripcion", {required: true})}
                    />
                  </Col>
                </FormGroup>


                <FormGroup row>
                  <Label for="lblPerfilAca" sm={3}>
                   Perfil acádemico
                  </Label>
                  <Col sm={9}>
                  <input
                      type="textarea"
                      className="form-control"
                      placeholder="Perfil acádemico"
                      {...register("perfil_academico", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblPerfilAca" sm={3}>
                  Experiencia laboral
                  </Label>
                  <Col sm={9}>
                  <input
                      type="textarea"
                      className="form-control"
                      placeholder="Experiencia laboral"
                      {...register("experiencia_laboral", {required: true})}
                    />
                  </Col>
                </FormGroup>



                <FormGroup row>
                  <Label for="lblPerfilAca" sm={3}>
                  Salario mínimo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="textarea"
                      className="form-control"
                      placeholder="Salario mínimo"
                      {...register("salario_minimo", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblPerfilAca" sm={3}>
                  Salario máximo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="textarea"
                      className="form-control"
                      placeholder="Salario máximo"
                      {...register("salario_maximo", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/btcurriculums') }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cCurriculum} >Modificar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtOfertaTrabajoeditar;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BthabdestrezasEliminar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;

  useEffect(() => {

    //Experiencia laboral
    const fCurriculum = async () => {
      try {
        axios.get( (getUrlBE() + '/habdestrezasid/'+idCV)).then(response => {
        
         setValue("habilidad_destreza",response.data.datos.habilidad_destrezas)
         setValue("nivel",response.data.datos.nivel)
         setData(response.data.datos.id_oferta_tarabajo)

        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fCurriculum()
  }, [])

  const cElaboral = () => {


    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('idcv', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/habdestrezaseliminar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro eliminado con éxito")
            props.history.push('/ofertaver/'+data)
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Eliminar"
      breadcrumbs={[{ name: 'Habilidades-Destrezas', active: true }]}
      className="habdestrezasTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Eliminar habilidades/destrezas

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblCompania" sm={3}>
                    Habilidad/Destreza
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      disabled
                      className="form-control"
                      placeholder="Habilidad/Destreza"
                      {...register("habilidad_destreza", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNivel" sm={3}>
                   Nivel
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      disabled
                      className="form-control"
                      placeholder="Establezca un nivel del 1 al 10"
                      {...register("nivel", {required: true})}
                    />
                  </Col>
                </FormGroup>

               
                
                
                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/ofertaver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="danger" onClick={cElaboral} >Eliminar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BthabdestrezasEliminar;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
} from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import { FaEdit } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";

import { FaPlusCircle } from "react-icons/fa";


import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";
var DatePicker = require("reactstrap-date-picker");



const BtOfertaTrabajoVer = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idOf = props.match.params.id;

  //Definiciones de variables para la informacion de la plaza
  const [oferta, setOferta] = useState([])
  const [habdestrezas, setHabdestrezas] = useState([])


  useEffect(() => {

    //Experiencia laboral
    const fOferta = async () => {
      try {
        axios.get( (getUrlBE() + '/oferta/'+idOf)).then(response => {

          setOferta(response.data.datos)

        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fOferta()

    const fHabdestrezas = async () => {
      try {
        axios.get( (getUrlBE() + '/habdestrezasall/'+idOf)).then(response => {

          setHabdestrezas(response.data.datos)

        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fHabdestrezas()

   

  }, [])
  

  return (
    <Page
      title="Plaza"
      breadcrumbs={[{ name: 'Plaza', active: true }]}
      className="OfertaTable"
    >

        <Row>
          <Col xl={6} lg={12} md={12} >
            <Card className="mb-3 bg-3">
              <CardHeader>Información general
              <Button className="float-right" onClick={() => { props.history.push('/ofertaeditar/'+idOf) }} ><FaEdit /></Button>
              </CardHeader>
              
            <CardBody>

            <p>Plaza: {oferta.oferta_trabajo}</p>
            <p>Estado: {oferta.estado_oferta}</p>
            <p>Salario minimo: {oferta.salario_min}</p>
            <p>Salario máximo: {oferta.salario_max}</p>
            <p>Locación</p>
            <p>Municipio: {oferta.municipio}</p>
            <p>Departamento: {oferta.departamento}</p>
            <p>Descripción: <br /> 
            {oferta.descripcion_puesto}</p>

            <p>Experiencia laboral: <br /> 
            {oferta.experiencia_laboral}</p>
            <p>Perfil acádemico: <br /> 
            {oferta.perfil_academico}</p>

            <p>Fecha de creación: <Moment format="YYYY/MM/DD">
                        {oferta.fecha_creacion}
                      </Moment></p>
            
            

            </CardBody>

            </Card>


          
          </Col>

          <Col xl={6} lg={12} md={12} >
          
            
            <Card className="mb-3 bg-3" id="card_habdestrezas">
              <CardHeader>
                <span>Habilidades / Destrezas</span>
                <Button className="float-right" onClick={() => { props.history.push('/habdestrezas/'+idOf) }} >
                  <FaPlusCircle />
                </Button>
              </CardHeader>
             
            </Card>

            {habdestrezas.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.habilidad_destrezas}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarhabdestrezas/'+ce.id_habilidad_destreza) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarhabdetrezas/'+ce.id_habilidad_destreza) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Nivel: {ce.nivel}</p>
                    
                  </CardText>
                  
                </CardBody>
                ))}






          </Col>
        </Row>


        <Row>
          <Col xl={6} lg={12} md={12} >

          
          </Col>
        </Row>


    </Page>
  );
};

export default BtOfertaTrabajoVer;

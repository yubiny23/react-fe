import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button ,
Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar
} from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";



const BtUsuariosaAgregar = () => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  
  useEffect(() => {
    const getCharacters = async () => {
        try {

          axios.get( getUrlBE() + '/rolesa').then(response => {
              
            setData( response.data.datos );
              
          }, []).catch(error => console.log(error))


        } catch(err) {
          // error handling code
        } 
    }
    getCharacters();
  }, []);


  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Administradores', active: true }]}
      className="UsuariosaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar administrador

              </CardHeader>
              
            <CardBody>
              <Form>
                <FormGroup row>
                  <Label for="lblNombres" sm={2}>
                    Nombres
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="text"
                      name="nombres"
                      placeholder="Ingrese sus nombres"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblApalleidos" sm={2}>
                    Apellidos
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="text"
                      name="lblApellidos"
                      placeholder="Ingrese sus apellidos"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblEmail" sm={2}>
                    Email
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="email"
                      name="email"
                      placeholder="admin@admin.com"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblPassword" sm={2}>
                    Password
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="password"
                      name="password"
                      placeholder="password"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblPassword2" sm={2}>
                    Coonfirmar Password
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="password"
                      name="password"
                      placeholder="Confirmar password placeholder"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleSelect" sm={2}>
                    Rol
                  </Label>
                  <Col sm={10}>
                  <Input type="select" {...register("rol", { required: true })} >
                    {data.map((r, index) => ( 
                      <option key={r.id_rol} value={r.id_rol}>{r.rol}</option>
                    ))}
                  </Input>
                  
                  </Col>
                </FormGroup>

                <ButtonToolbar>
                <ButtonGroup className="mr-3 mb-3">
                 
                    <Button color="secondary">Cancelar</Button>
                
                </ButtonGroup>
                <ButtonGroup className="mr-3 mb-3">
                 
                 <Button color="primary">Guardar</Button>
             
                </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtUsuariosaAgregar;

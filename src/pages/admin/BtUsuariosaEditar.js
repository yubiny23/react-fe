import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button ,
Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar
} from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";



const BtUsuariosEditar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const id = props.match.params.id;
  
  useEffect(() => {
    const getCharacters = async () => {
        try {

          axios.get( getUrlBE() + '/rolesa').then(response => {
              
            setData( response.data.datos );
              
          }, []).catch(error => console.log(error))


        } catch(err) {
          // error handling code
        } 
    }
    getCharacters();
  }, []);


  useEffect(() => {
    const fetchData = async () => {
      try {

        axios.get( (getUrlBE() + '/usuariosme/'+id)).then(response => {
            
          setValue('nombres', response.data.nombres);
          setValue('apellidos', response.data.apellidos);
          setValue('correo', response.data.correo_electronico);
          setValue("rol", response.data.id_rol);
            
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
    fetchData()
    
   
  }, [])

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'Administradores', active: true }]}
      className="UsuariosaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar administrador

              </CardHeader>
              
            <CardBody>
              <Form>
                <FormGroup row>
                  <Label for="lblNombres" sm={2}>
                    Nombres
                  </Label>
                  <Col sm={10}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese sus nombres"
                      {...register("nombres", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblApalleidos" sm={2}>
                    Apellidos
                  </Label>
                  <Col sm={10}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese sus Apallidos"
                      {...register("apellidos", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblEmail" sm={2}>
                    Email
                  </Label>
                  <Col sm={10}>
                  <input
                      type="email"
                      className="form-control"
                      placeholder="Ingrese su correo"
                      {...register("correo", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="exampleSelect" sm={2}>
                    Rol
                  </Label>
                  <Col sm={10}>
                  <select className="form-control" {...register("rol", { required: true })} >
                    {data.map((r, index) => ( 
                      <option key={r.id_rol} value={r.id_rol}>{r.rol}</option>
                    ))}
                  </select>
                  
                  </Col>
                </FormGroup>

                
                

                <ButtonToolbar>
                <ButtonGroup className="mr-3 mb-3">
                 
                    <Button color="secondary">Cancelar</Button>
                
                </ButtonGroup>
                <ButtonGroup className="mr-3 mb-3">
                 
                 <Button color="primary">Guardar</Button>
             
                </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtUsuariosEditar;

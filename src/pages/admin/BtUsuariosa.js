import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  




const BtUsuariosa = () => {

  const [data, setData] = useState([])
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios.get( (getUrlBE() + '/usuariosm')).then(response => {
        setData(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
  
    // call the async fetchData function
    fetchData()
  
  }, [])


  return (
    <Page
      title="Administradores"
      breadcrumbs={[{ name: 'Administradores', active: true }]}
      className="UsuariosaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Listado de usuarios administradores
                <Link to={"/agregarusuario"} className="btn btn-primary float-right">Agregar</Link>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ ['striped']: true }}>
                        <thead>
                        <tr>
                            <th>Correo</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Fecha creacion</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                          
                          
                          {data.map((user, index) => ( 

                            <tr>
                            <td scope="row">{user.correo}</td>
                            <td >{user.nombres}</td>
                            <td>{user.apellidos}</td>
                            <td>{user.fecha_creacion}</td>
                            <td><Link to={"/editarusuario/"+user.id_usuario}  className="nav-link">
                                Editar
                              </Link> | 
                            <Link to={"/eliminarusuario/"+user.id_usuario} className="nav-link">
                                Eliminar
                              </Link>
                            </td>
                            </tr>

                          ))}

                            
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtUsuariosa;

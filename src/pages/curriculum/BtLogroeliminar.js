import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtLogroeliminar = (props) => {

  const [fechai, setFechai] = useState([])
  const [fechaf, setFechaf] = useState([])
  const [idCV, setIdcv] = useState([])
  const [tipologro, setTipologro] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idl = props.match.params.id;
   

  useEffect(() => {
    const getTiposLogro = async () => {
        try {

          axios.get( getUrlBE() + '/tipologro').then(response => {
              
            setTipologro( response.data.datos );
              
          }, []).catch(error => console.log(error))


        } catch(err) {
          // error handling code
        } 
    }
    getTiposLogro();
  



  //Experiencia laboral
  const fCertificaciones = async () => {
    try {
      axios.get( (getUrlBE() + '/logroid/'+idl)).then(response => {

        var fi = moment(response.data.datos.fecha_inicio).format("YYYY-MM-DD") 
        var ff = moment(response.data.datos.fecha_fin).format("YYYY-MM-DD") 
       
        setFechai(fi)
        setFechaf(ff)

        setValue("logro",response.data.datos.logro)
        setValue("tipo_logro",response.data.datos.id_tipo_logro)


        setIdcv(response.data.datos.id_curriculum)

      }, []).catch(error => console.log(error))
    } catch(err) {
      // error handling code
    }
  }
  fCertificaciones()

}, []);

  const cLogro = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('id', idl);

    axios.post( 
      (
        
        getUrlBE() + '/logroeliminar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro eliminado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

 

    
  

  return (
    <Page
      title="Eliminar"
      breadcrumbs={[{ name: 'Logro', active: true }]}
      className="curriculumTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Eliminar logro

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                    Logro
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Logro"
                      {...register("logro", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblInstitucion" sm={3}>
                    Tipo de Logro
                  </Label>
                  <Col sm={9}>
                  <Input type="select" {...register("tipo_logro", { required: true })} >
                    {tipologro.map((r, index) => ( 
                      <option key={r.id_tipo_logro} value={r.id_tipo_logro}>{r.tipo_logro}</option>
                    ))}
                  </Input>
                  </Col>
                </FormGroup>
                

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de inicio
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-finicio" 
                    dateFormat="YYYY-MM-DD"
                    value={fechai.toString()}
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lbl-ffin" sm={3}>
                    Fecha de Finalizaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-ffin" 
                    dateFormat="YYYY-MM-DD"
                    value={fechaf.toString()}
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
               

                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="danger" onClick={cLogro} >Eliminar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtLogroeliminar;

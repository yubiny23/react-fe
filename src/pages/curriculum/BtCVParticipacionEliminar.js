import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCVParticipacionEliminar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const id = props.match.params.id;
  const [ofertalaboral, setOfertalaboral] = useState([])
  const [plaza, setPlaza] = useState([])
  const [idCv, setCv] = useState([])

  const cPariticpacion = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('comentario', getValues('comentario'));
    bodyFormData.append('id', id);

    axios.post( 
      (
        
        getUrlBE() + '/participacioneliminar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro Eliminado con éxito")
            props.history.push('/curriculumver/'+idCv)
        }else{

        }

    }).catch(error => {

    });
    
  }

   
  useEffect(() => {
  

    const fParticpacion = async () => {
      try {
        axios.get( (getUrlBE() + '/participacionid/'+id)).then(response => {
    
          setData(response.data.datos)
          setValue("comentario",response.data.datos.comentarios)
          setCv(response.data.datos.id_curriculum)
    
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fParticpacion()
  
  
  }, []);



  

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'Participación', active: true }]}
      className="CertificacionTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Editar participación

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                   Plaza
                  </Label>
                  <Col sm={9}>
                    {data.oferta_trabajo}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                   Descripción del puesto
                  </Label>
                  <Col sm={9}>
                    {data.descripcion_puesto}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblComentarios" sm={3}>
                    Comentarios
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      disabled
                      placeholder="Comentarios"
                      {...register("comentario", {required: true})}
                    />
                  </Col>
                </FormGroup>

                
                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCv) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="danger" onClick={cPariticpacion} >Eliminar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCVParticipacionEliminar;

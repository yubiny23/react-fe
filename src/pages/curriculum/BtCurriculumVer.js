import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
} from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import { FaEdit } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";

import { FaPlusCircle } from "react-icons/fa";


import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";
var DatePicker = require("reactstrap-date-picker");



const BtCurriculumVer = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;

  //Definiciones de variables para la informacion del CV
  const [conAcademicos, setConAcademicos] = useState([])
  const [expLaboral, setExpLaboral] = useState([])
  const [idiomas, setIdiomas] = useState([])
  const [certificaciones, setCertificaciones] = useState([])
  const [publicaciones, setPublicaciones] = useState([])
  const [logros, setLogros] = useState([])
  const [redsocial, setRedsocial] = useState([])
  const [reclaboral, setReclaboral] = useState([])
  const [recpersonal, setRecpersonal] = useState([])
  const [participacion, setParticipacion] = useState([])

  const [curriculum, setCurriculum] = useState([])

  useEffect(() => {

    
    //Experiencia laboral
    const fCurriculum = async () => {
      try {
        axios.get( (getUrlBE() + '/curriculum/'+idCV)).then(response => {

          setCurriculum(response.data.datos)

        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fCurriculum()

    //Conocimientos academicos
    const fConocimientosAdemicos = async () => {
      try {
        axios.get( (getUrlBE() + '/conocimientosacademicos/'+idCV)).then(response => {
          setConAcademicos(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fConocimientosAdemicos()
  
    //Experiencia laboral
    const fExperienciaLbaoral = async () => {
      try {
        axios.get( (getUrlBE() + '/experiencialaoral/'+idCV)).then(response => {
          setExpLaboral(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fExperienciaLbaoral()
  
    //Experiencia laboral
    const fIdiomas = async () => {
      try {
        axios.get( (getUrlBE() + '/idiomasall/'+idCV)).then(response => {
          setIdiomas(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fIdiomas()
  
    //Experiencia laboral
    const fCertificaciones = async () => {
      try {
        axios.get( (getUrlBE() + '/certificacionesall/'+idCV)).then(response => {
          setCertificaciones(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fCertificaciones()
  
    //Publicaciones
    const fPublicaciones = async () => {
      try {
        axios.get( (getUrlBE() + '/publicacionesall/'+idCV)).then(response => {
          setPublicaciones(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fPublicaciones()
  
  
    //Experiencia laboral
    const fLogros = async () => {
      try {
        axios.get( (getUrlBE() + '/logrosall/'+idCV)).then(response => {
          setLogros(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fLogros()

    //Experiencia laboral
    const fRedsocial = async () => {
      try {
        axios.get( (getUrlBE() + '/redsocialall/'+idCV)).then(response => {
          setRedsocial(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fRedsocial()


    //Experiencia laboral
    const fReclaboral = async () => {
      try {
        axios.get( (getUrlBE() + '/recomendacionlaboralall/'+idCV)).then(response => {
          setReclaboral(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fReclaboral()

    //Experiencia laboral
    const fRecpersonal = async () => {
      try {
        axios.get( (getUrlBE() + '/recomendacionpersonalall/'+idCV)).then(response => {
          setRecpersonal(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fRecpersonal()

    //Participaciones
    const fParticipacion = async () => {
      try {
        axios.get( (getUrlBE() + '/participacionesall/'+idCV)).then(response => {
          setParticipacion(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fParticipacion()


  }, [])
  

  return (
    <Page
      title="Curriculum"
      breadcrumbs={[{ name: 'Curriculum', active: true }]}
      className="CurriculumTable"
    >

        <Row>
          <Col xl={6} lg={12} md={12} >
            <Card className="mb-3 bg-3">
              <CardHeader>Información general
              <Button className="float-right" onClick={() => { props.history.push('/editarcurriculum/'+idCV) }} ><FaEdit /></Button>
              </CardHeader>
              
            <CardBody>

            <p>Nombres: {curriculum.nombres}</p>
            <p>Apellidos: {curriculum.apellidos}</p>
            <p>Correo elect&oacute;nico: {curriculum.correo_electronico}</p>
            <p>Direcci&oacute;n: {curriculum.direccion}</p>
            <p>Genero: {curriculum.genero}</p>
            <p>Fecha de nacimiento: <Moment format="YYYY/MM/DD">
                        {curriculum.fecha_nacimiento}
                      </Moment></p>
            <p>DUI: {curriculum.dui_pasaporte}</p>
            <p>NIT: {curriculum.nit}</p>
            <p>NUP: {curriculum.nup}</p>

            

            </CardBody>

            </Card>

            <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Certificaciones</span>
                <Button className="float-right"  onClick={() => { props.history.push('/agregarcrt/'+idCV) }}  ><FaPlusCircle /></Button>
              </CardHeader>
              
            <CardBody>

            {certificaciones.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.certificacion}</Badge></h2></Col>
                    <Col>

                    <ButtonToolbar className="float-right" >

                    <ButtonGroup className="mr-2">
                      <Button className="btn btn-success"  onClick={() => { props.history.push('/editarcrt/'+ce.id_certificacion) }} ><FaEdit /></Button>
                    </ButtonGroup>

                    <ButtonGroup className="mr-2">
                    < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarcrt/'+ce.id_certificacion) }} ><FaTrash /></Button>
                    </ButtonGroup>

                   </ButtonToolbar>

 


                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Instituci&oacute;n: {ce.institucion}</p>
                    <p>C&oacute;digo: {ce.codigo_certificacion}</p>
                    <p>Tipo: {ce.tipo_certificacion}</p>
                    <p>
                      <Moment format="YYYY/MM/DD">
                        {ce.fecha_inicio}
                      </Moment>
                      - 
                      <Moment format="YYYY/MM/DD">
                        {ce.fecha_fin}
                      </Moment>
                    </p>
                  </CardText>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>


          <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Publicaciones</span>
                
                <Button className="float-right" onClick={() => { props.history.push('/agregarpublicacion/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>
              
            <CardBody>

            {publicaciones.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.publicacion}</Badge></h2></Col>
                    <Col>
                    
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarpublicacion/'+ce.id_publicacion) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarpublicacion/'+ce.id_publicacion) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>

                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Lugar: {ce.lugar_publicacion}</p>
                    <p>C&oacute;digo: {ce.codigo_certificacion}</p>
                    <p>Libro/Publicaci&oacute;n: {ce.es_libro_edicion}</p>
                    <p>ISBN: {ce.isbn}</p>
                    <p> Fecha de publicaci&oacute;n
                      <Moment format="YYYY/MM/DD">
                        {ce.fecha_publicacion}
                      </Moment>
                    </p>
                  </CardText>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>


          <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Logros</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarlogro/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>             
            <CardBody>

            {logros.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.tipo_logro}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarlogro/'+ce.id_curriculum_x_logro) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarlogro/'+ce.id_curriculum_x_logro) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Logro: {ce.logro}</p>

                    <p> Fecha de Inicio: &nbsp;  
                      <Moment format="YYYY/MM/DD">
                        {ce.fecha_inicio}
                      </Moment>
                    </p>

                    <p> Fecha de Fin: &nbsp;
                      <Moment format="YYYY/MM/DD">
                        {ce.fecha_fin}
                      </Moment>
                    </p>

                  </CardText>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>


          <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Red Social</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarrs/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>
              
            <CardBody>

            {redsocial.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.red_social}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarrs/'+ce.id_curriculum_rs) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>

          
          </Col>

          <Col xl={6} lg={12} md={12} >
          
          <Card className="mb-3 bg-3" id="card_participaciones">
              <CardHeader>
                <span>Participaciones en las plazas</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarparticipacion/'+idCV) }} >
                  <FaPlusCircle />
                </Button>
              </CardHeader>
              
              {participacion.map((ca, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ca.oferta_trabajo}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarparticipacion/'+ca.id_participacion) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarparticipacion/'+ca.id_participacion) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Comentarios:
                      <br />
                      {ca.comentarios}
                    </p>
                    <p>Fecha de participación: &nbsp;
                      <Moment format="YYYY/MM/DD">
                        {ca.fecha_creacion}
                      </Moment>
                    </p>
                  </CardText>
                  
                </CardBody>
                ))}
            </Card>
            
            <Card className="mb-3 bg-3" id="card_conocimientosacademicos">
              <CardHeader>
                <span>Conocimientos acad&eacute;micos</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarcademicos/'+idCV) }} >
                  <FaPlusCircle />
                </Button>
              </CardHeader>
              
              {conAcademicos.map((ca, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ca.titulo}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarcademicos/'+ca.id_conocimiento_academico) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarcacademicos/'+ca.id_conocimiento_academico) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>{ca.institucion}</p>
                    <p>
                      <Moment format="YYYY/MM/DD">
                        {ca.fecha_inicio}
                      </Moment>
                      - 
                      <Moment format="YYYY/MM/DD">
                        {ca.fecha_fin}
                      </Moment>
                    </p>
                  </CardText>
                  
                </CardBody>
                ))}
            </Card>

            <Card className="mb-3 bg-3" id="card_experiencialaboral">
            <CardHeader>
                <span>Experiencia laboral</span><Button className="float-right" onClick={() => { props.history.push('/agregarelaboral/'+idCV) }}  ><FaPlusCircle /></Button>
              </CardHeader>
              
              {expLaboral.map((ca, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col>
                      <h4>{ca.compania}</h4>
                     
                    </Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarelaboral/'+ca.id_experiencia_laboral) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarelaboral/'+ca.id_experiencia_laboral) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                  <p>{ca.funciones}</p>
                  <hr></hr>
                    <p>Contacto: {ca.nombre_contrato}</p>
                    <p>Posici&oacute;n: {ca.posicion_contrato}</p>
                    <p>Tel 1: {ca.telefono_contacto}</p>
                    <p>Tel 2: {ca.telefono_contacto2}</p>
                    <p>
                      <Moment format="YYYY/MM/DD">
                        {ca.fecha_inicio}
                      </Moment>
                      - 
                      <Moment format="YYYY/MM/DD">
                        {ca.fecha_fin}
                      </Moment>
                    </p>
                  </CardText>
                  
                </CardBody>
                ))}
            </Card>

            <Card className="mb-3 bg-3" id="card_experiencialaboral">
            <CardHeader>
                <span>Lenguajes</span><Button className="float-right" onClick={() => { props.history.push('/agregarlenguaje/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>
              
              {idiomas.map((ca, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col>
                      <h4>Idioma: {ca.lenguaje}</h4>
                     
                    </Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarlenguaje/'+ca.id_curriculum_x_lenguaje) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarlenguaje/'+ca.id_curriculum_x_lenguaje) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Conversaci&oacute;n: {ca.conversacion}</p>
                    <p>Escritura: {ca.escritura}</p>
                    <p>Escucha: {ca.escucha}</p>
                    <p>Lectura: {ca.lectura}</p>
                  </CardText>
                  
                </CardBody>
                ))}
                
            </Card>


            <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Recomendaci&oacute;n laboral</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarreclaboral/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>
              
            <CardBody>

            {reclaboral.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.nombre_contacto}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarreclaboral/'+ce.id_recomendacion_laboral) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarreclaboral/'+ce.id_recomendacion_laboral) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Institucion: {ce.institucion}</p>
                    <p>Tel&eacute;fono: {ce.telefono_contacto}</p>
                    <p>Tel&eacute;fono: {ce.telefono_contacto2}</p>
                  </CardText>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>

          <Card className="mb-3 bg-3">
              <CardHeader>
                <span>Recomendaci&oacute;n personal</span>
                <Button className="float-right" onClick={() => { props.history.push('/agregarrecpersonal/'+idCV) }} ><FaPlusCircle /></Button>
              </CardHeader>
              
            <CardBody>

            {recpersonal.map((ce, index) => ( 
                <CardBody key={index}  >
                <CardTitle>
                  <Row>
                    <Col><h2><Badge color="secondary">{ce.nombre_contacto}</Badge></h2></Col>
                    <Col>
                    <ButtonToolbar className="float-right" >

                      <ButtonGroup className="mr-2">
                        <Button className="btn btn-success"  onClick={() => { props.history.push('/editarrecpersonal/'+ce.id_recomendacion_personal) }} ><FaEdit /></Button>
                      </ButtonGroup>

                      <ButtonGroup className="mr-2">
                      < Button className="btn btn-danger" onClick={() => { props.history.push('/eliminarrecpersonal/'+ce.id_recomendacion_personal) }} ><FaTrash /></Button>
                      </ButtonGroup>

                    </ButtonToolbar>
                    </Col>
                  </Row>
                  
                </CardTitle>
                  <CardText>
                    <p>Tel&eacute;fono: {ce.telefono_contacto}</p>
                    <p>Tel&eacute;fono: {ce.telefono_contacto2}</p>
                  </CardText>
                  
                </CardBody>
                ))}
              
            </CardBody>
          </Card>




          </Col>
        </Row>


        <Row>
          <Col xl={6} lg={12} md={12} >

          
          </Col>
        </Row>


    </Page>
  );
};

export default BtCurriculumVer;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtRseliminar = (props) => {

  
  const [rs, setRs] = useState([]) // variables para poblar el select de Redes sociales
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;
   

  useEffect(() => {
  

  const getCharacters = async () => {
      try {

        axios.get( getUrlBE() + '/rsall').then(response => {
            
          setRs( response.data.datos );
            
        }, []).catch(error => console.log(error))


      } catch(err) {
        // error handling code
      } 
  }
  getCharacters();


}, []);

  const cRs = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('redsocial', getValues("red_social_cv"))
    bodyFormData.append('id', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/redsocialagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro agregado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

 

    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Red social', active: true }]}
      className="redsocialTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar Red Social

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                    Red social
                  </Label>
                  <Col sm={9}>

                  <Input type="select" {...register("red_social_cv", { required: true })} >
                  <option key={0} value={""}>Selecciona una opción</option>
                    {rs.map((r, index) => ( 
                      <option key={r.id_red_social} value={r.id_red_social}>{r.red_social}</option>
                    ))}
                  </Input>
                    
                  </Col>
                </FormGroup>
              
                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cRs} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtRseliminar;

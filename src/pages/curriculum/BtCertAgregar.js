import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCertAgregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;

  const cCertificacion = () => {

    const fechaIni = document.getElementById("dp-finicio").value
    const fechaFin = document.getElementById("dp-ffin").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('certificacion', getValues('certificacion') );
    bodyFormData.append('institucion', getValues('institucion'));
    bodyFormData.append('codigo', getValues('codigo'));
    bodyFormData.append('tipo', getValues('tipo'));
    bodyFormData.append('fecha_inicio', fechaIni);
    bodyFormData.append('fecha_fin', fechaFin);
    bodyFormData.append('idcv', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/certificacionagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Empresa', active: true }]}
      className="EmpresaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar certificaci&oacute;n

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblCertificacion" sm={3}>
                    Cerificaci&oacute;n
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Certificaci&oacute;n"
                      {...register("certificacion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblInstitucion" sm={3}>
                    Instituci&oacute;n
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Instituci&oacute;n"
                      {...register("institucion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblCorreo" sm={3}>
                    Co&oacute;digo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Co&oacute;digo"
                      {...register("codigo", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTipo" sm={3}>
                    Tipo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tipo"
                      {...register("tipo", {required: true})}
                    />
                  </Col>
                </FormGroup>


                

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de inicio
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-finicio" 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lbl-ffin" sm={3}>
                    Fecha de Finalizaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-ffin" 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
               

                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cCertificacion} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCertAgregar;

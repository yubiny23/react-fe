import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCacademicoAgregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;
  const [ofertalaboral, setOfertalaboral] = useState([])
  

  const cPariticpacion = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('id_oferta_trabajo', getValues('plaza') );
    bodyFormData.append('comentario', getValues('comentario'));
    bodyFormData.append('idcv', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/participacionagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

   
  useEffect(() => {
  

    const getOfertalaboral = async () => {
        try {

          
          axios.get( getUrlBE() + '/ofertaslaboralall/'+idCV).then(response => {
              
            setOfertalaboral( response.data.datos );
              
          }, []).catch(error => console.log(error))
  
  
        } catch(err) {
          // error handling code
        } 
    }
    getOfertalaboral();
  
  
  }, []);
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Participación', active: true }]}
      className="CertificacionTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar participación

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                   Plaza
                  </Label>
                  <Col sm={9}>

                  <Input type="select" {...register("plaza", { required: true })} >
                  <option key={0} value={""}>Selecciona una opción</option>
                    {ofertalaboral.map((r, index) => ( 
                      <option key={r.id_oferta_trabajo} value={r.id_oferta_trabajo}>{r.oferta_trabajo}</option>
                    ))}
                  </Input>
                    
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblComentarios" sm={3}>
                    Comentarios
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Comentarios"
                      {...register("comentario", {required: true})}
                    />
                  </Col>
                </FormGroup>

                
                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cPariticpacion} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCacademicoAgregar;

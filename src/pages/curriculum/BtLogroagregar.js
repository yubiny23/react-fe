import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtLogroAgregar = (props) => {

  const [data, setData] = useState([])
  const [tipologro, setTipologro] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;

  const cLogro = () => {

    const fechaIni = document.getElementById("dp-finicio").value
    const fechaFin = document.getElementById("dp-ffin").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('logro', getValues('logro') );
    bodyFormData.append('tipo_logro', getValues('tipo_logro'));
    bodyFormData.append('fecha_inicio', fechaIni);
    bodyFormData.append('fecha_fin', fechaFin);
    bodyFormData.append('idcv', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/logroagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

  useEffect(() => {
    const getTiposLogro = async () => {
        try {

          axios.get( getUrlBE() + '/tipologro').then(response => {
              
            setTipologro( response.data.datos );
              
          }, []).catch(error => console.log(error))


        } catch(err) {
          // error handling code
        } 
    }
    getTiposLogro();
  }, []);


    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Logro', active: true }]}
      className="EmpresaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar logro

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblCertificacion" sm={3}>
                    Logro
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Logro"
                      {...register("logro", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblInstitucion" sm={3}>
                    Tipo de Logro
                  </Label>
                  <Col sm={9}>
                  <Input type="select" {...register("tipo_logro", { required: true })} >
                    {tipologro.map((r, index) => ( 
                      <option key={r.id_tipo_logro} value={r.id_tipo_logro}>{r.tipo_logro}</option>
                    ))}
                  </Input>
                  </Col>
                </FormGroup>
                

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de inicio
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-finicio" 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lbl-ffin" sm={3}>
                    Fecha de Finalizaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-ffin" 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
               

                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cLogro} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtLogroAgregar;

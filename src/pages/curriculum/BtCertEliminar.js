import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCertEliminar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idC = props.match.params.id;
  const [idCV, setIdcv] = useState([])

  const [fechai, setFechai] = useState([])
  const [fechaf, setFechaf]= useState([])

  //Experiencia laboral
  const fCertificaciones = async () => {
    try {
      axios.get( (getUrlBE() + '/certificacionesid/'+idC)).then(response => {

        var fi = moment(response.data.datos.fecha_inicio).format("YYYY-MM-DD") 
        var ff = moment(response.data.datos.fecha_fin).format("YYYY-MM-DD") 
       
        setFechai(fi)
        setFechaf(ff)

        
       
        setValue("certificacion",response.data.datos.certificacion)
        setValue("institucion",response.data.datos.institucion)
        setValue("codigo",response.data.datos.codigo_certificacion)
        setValue("tipo",response.data.datos.tipo_certificacion)


        setIdcv(response.data.datos.id_persona)

      }, []).catch(error => console.log(error))
    } catch(err) {
      // error handling code
    }
  }
  fCertificaciones()

  const cCertificacion = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('idc', idC);

    axios.post( 
      (
        
        getUrlBE() + '/certificacioneliminar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro Eliminado con éxito")
            props.history.push('/curriculumver/'+idCV )
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Eliminar"
      breadcrumbs={[{ name: 'Certificación', active: true }]}
      className="curriculumTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Eliminar certificaci&oacute;n
              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblCertificacion" sm={3}>
                    Cerificaci&oacute;n
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Certificaci&oacute;n"
                      disabled
                      {...register("certificacion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblInstitucion" sm={3}>
                    Instituci&oacute;n
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Instituci&oacute;n"
                      disabled
                      {...register("institucion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblCorreo" sm={3}>
                    Co&oacute;digo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Co&oacute;digo"
                      disabled
                      {...register("codigo", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTipo" sm={3}>
                    Tipo
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tipo"
                      disabled
                      {...register("tipo", {required: true})}
                    />
                  </Col>
                </FormGroup>


                

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de inicio
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-finicio" 
                    dateFormat="YYYY-MM-DD"
                    value={fechai.toString()}
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    disabled
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lbl-ffin" sm={3}>
                    Fecha de Finalizaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-ffin" 
                    dateFormat="YYYY-MM-DD"
                    value={fechaf.toString()}
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    disabled
                    />
                  </Col>
                </FormGroup>
               

                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="danger" onClick={cCertificacion} >Eliminar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCertEliminar;

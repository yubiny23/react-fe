import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCurriculumAgregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  

  const cCurriculum = () => {

    const fecha = document.getElementById("example-datepicker").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('nombres', getValues('nombres') );
    bodyFormData.append('apellidos', getValues('apellidos'));
    bodyFormData.append('direccion', getValues('direccion'));
    bodyFormData.append('genero', getValues('genero'));
    bodyFormData.append('fecha_nacimiento',  fecha );
    bodyFormData.append('correo_electronico', getValues('correo_electronico'));
    bodyFormData.append('dui_pasaporte', getValues('dui_pasaporte'));
    bodyFormData.append('nit', getValues('nit'));
    bodyFormData.append('nup', getValues('nup'));

    axios.post( 
      (
        
        getUrlBE() + '/curriculumagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+response.data.datos.id)
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Empresa', active: true }]}
      className="EmpresaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar administrador

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblNombres" sm={3}>
                    Nombres
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nombres"
                      {...register("nombres", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblApellidos" sm={3}>
                    Apellidos
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Apellidos"
                      {...register("apellidos", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblCorreo" sm={3}>
                    Correo electr&oacute;nico
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="test@test.com"
                      {...register("correo_electronico", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNup" sm={3}>
                    Direcci&oacute;n
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="direccion"
                      {...register("direccion", {required: true})}
                    />
                  </Col>
                </FormGroup>


                <FormGroup row>
                  <Label for="lbl-genero" sm={3}>
                    Genero
                  </Label>
                  <Col sm={9}>
                  <Input type="select" {...register("genero", { required: true })} >
                      <option key={"G1"} value={""}>{"Seleccione uno"}</option>
                      <option key={"M"} value={"M"}>{"M"}</option>
                      <option key={"F"} value={"F"}>{"F"}</option>
                  </Input>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lbl-telefono_1" sm={3}>
                    Fecha de nacimiento
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 

                    id      = "example-datepicker" 

                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                   
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lblDui" sm={3}>
                    DUI
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="DUI"
                      {...register("dui_pasaporte", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNit" sm={3}>
                    NIT
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="NIT"
                      {...register("nit", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNup" sm={3}>
                    NUP
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="NUP"
                      {...register("nup", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/btcurriculums') }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cCurriculum} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCurriculumAgregar;

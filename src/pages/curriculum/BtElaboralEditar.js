import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");




const BtElaboraloEditar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idC = props.match.params.id;
  const [idCV, setIdcv] = useState([])

  const [fechai, setFechai] = useState([])
  const [fechaf, setFechaf]= useState([])


  const cElaboral = () => {

    const fechaIni = document.getElementById("dp-finicio").value
    const fechaFin = document.getElementById("dp-ffin").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('compania', getValues('compania') );
    bodyFormData.append('funciones', getValues('funciones'));
    bodyFormData.append('contacto', getValues('nombre_contrato'));
    bodyFormData.append('posicion_contacto', getValues('posicion_contrato'));
    bodyFormData.append('telefono_contacto', getValues('telefono_contacto'));
    bodyFormData.append('telefono_contacto2', getValues('telefono_contacto2'));
    bodyFormData.append('fecha_inicio', fechaIni);
    bodyFormData.append('fecha_fin', fechaFin);
    bodyFormData.append('id', idC);

    axios.post( 
      (
        
        getUrlBE() + '/elaboraleditar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro modificado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }


  useEffect(() => {
    const fElaboral = async () => {
      try {
        axios.get( (getUrlBE() + '/experiencialaoralid/'+idC)).then(response => {
    
          var fi = moment(response.data.datos.fecha_inicio).format("YYYY-MM-DD") 
          var ff = moment(response.data.datos.fecha_fin).format("YYYY-MM-DD") 
        
          setFechai(fi)
          setFechaf(ff)
    
          
        
          setValue("compania",response.data.datos.compania)
          setValue("funciones",response.data.datos.funciones)
          setValue("nombre_contrato",response.data.datos.nombre_contrato)
          setValue("funciones_contrato",response.data.datos.funciones_contrato)
          setValue("posicion_contrato",response.data.datos.posicion_contrato)
          setValue("telefono_contacto",response.data.datos.telefono_contacto)
          setValue("telefono_contacto2",response.data.datos.telefono_contacto2)

    
          setIdcv(response.data.datos.id_persona)
    
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fElaboral()
  });

    
  

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'Experiencia laboral', active: true }]}
      className="ElaboralTable"
    >
        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Editar experiencia laboral

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblCompania" sm={3}>
                    Compa&ntilde;ia
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Compañia"
                      {...register("compania", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblFunciones" sm={3}>
                    Funciones
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Funciones"
                      {...register("funciones", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblContacto" sm={3}>
                    Cont&aacute;cto
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Cont&aacute;cto"
                      {...register("nombre_contrato", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblPosicion" sm={3}>
                    Posici&oacute;n
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Posici&oacute;n"
                      {...register("posicion_contrato", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTelefono" sm={3}>
                    Tel&eacute;fono
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tel&eacute;fono"
                      {...register("telefono_contacto", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTelefono" sm={3}>
                    Tel&eacute;fono 2
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tel&eacute;fono 2"
                      {...register("telefono_contacto2", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de inicio
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-finicio"
                    value={fechai.toString()} 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lbl-ffin" sm={3}>
                    Fecha de Finalizaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-ffin"
                    value={fechaf.toString()} 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>
                

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cElaboral} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtElaboraloEditar;

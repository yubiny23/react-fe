import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtRseliminar = (props) => {

  const [idCV, setIdcv] = useState([])
  const [rs, setRs] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idrs = props.match.params.id;
   

  useEffect(() => {
  //Red social
  const fRedsocial = async () => {
    try {
      axios.get( (getUrlBE() + '/redsocialid/'+idrs)).then(response => {

        setRs(response.data.datos.red_social)        
        setIdcv(response.data.datos.id_curriculum)

      }, []).catch(error => console.log(error))
    } catch(err) {
      // error handling code
    }
  }
  fRedsocial()

}, []);

  const cRs = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('id', idrs);

    axios.post( 
      (
        
        getUrlBE() + '/redsocialeliminar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro eliminado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

 

    
  

  return (
    <Page
      title="Eliminar"
      breadcrumbs={[{ name: 'Red social', active: true }]}
      className="redsocialTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Eliminar Red Social

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                    Red social
                  </Label>
                  <Col sm={9}>
                    {rs}
                  </Col>
                </FormGroup>
              
                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="danger" onClick={cRs} >Eliminar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtRseliminar;

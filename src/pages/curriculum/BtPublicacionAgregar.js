import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtPublicacionAgregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;
  

  const cPublicacion = () => {

    const fechaPub = document.getElementById("dp-fpublicacion").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('publicacion', getValues('publicacion') );
    bodyFormData.append('lugar', getValues('lugar'));
    bodyFormData.append('libro', getValues('libro'));
    bodyFormData.append('isbn', getValues('isbn'));
    bodyFormData.append('fecha_publicacion', fechaPub);
    bodyFormData.append('idcv', idCV);


    axios.post( 
      (
        
        getUrlBE() + '/publicaciongregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Publicación', active: true }]}
      className="CurriculumTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar publicaci&oacute;n

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblPublicacion" sm={3}>
                    Publicaci&oacute;n
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Publicaci&oacute;n"
                      {...register("publicacion", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblLugar" sm={3}>
                    Lugar
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Lugar"
                      {...register("lugar", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblLibro" sm={3}>
                    Libro
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Libro/Publicaci&oacute;n"
                      {...register("libro", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblIsbn" sm={3}>
                    ISBN
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="ISBN"
                      {...register("isbn", {required: true})}
                    />
                  </Col>
                </FormGroup>


                

                <FormGroup row>
                  <Label for="lbl-finicio" sm={3}>
                    Fecha de publicaci&oacute;n
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-fpublicacion" 
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                    />
                  </Col>
                </FormGroup>


                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cPublicacion} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtPublicacionAgregar;

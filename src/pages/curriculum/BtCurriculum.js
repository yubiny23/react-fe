import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  


const BTCurriculum = () => {

  const [data, setData] = useState([])
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios.get( (getUrlBE() + '/curriculumall') , { withCredentials: true }   ).then(response => {
        setData(response.data.datos)
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      } 
    }
  
    // call the async fetchData function
    fetchData()
  
  }, [])


  return (
    <Page
      title="Curriculum"
      breadcrumbs={[{ name: 'Curriculum', active: true }]}
      className="CurriculumTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Listado de Curriculums
                <Link to={"/agregarcurriculum"} className="btn btn-primary float-right">Agregar</Link>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ ['striped']: true }}>
                        <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>DUI</th>
                            <th>Correo electr&oacute;nico</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody> 
                          
                          
                          
                          {data.map((cv, index) => ( 

                            <tr>
                            <td scope="row">{cv.nombres}</td>
                            <td >{cv.apellidos}</td>
                            <td>{cv.dui_pasaporte}</td>
                            <td>{cv.correo_electronico}</td>
                            <td>
                              <Link to={"/curriculumver/"+cv.id_curriculum}  className="nav-link">
                                Ver
                              </Link>
                              
                            <Link to={"/curriculumeliminar/"+cv.id_curriculum} className="nav-link">
                                Eliminar
                              </Link>

                            </td>
                            </tr>

                          ))}

                            
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BTCurriculum;

import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import moment from "moment";

import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtCurriculumEditar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;
  const [curriculum, setCurriculum] = useState([])
  const [fecha, setFecha] = useState([])
  
  
  //setFecha(new Date());

  useEffect(() => {

    //Experiencia laboral
    const fCurriculum = async () => {
      try {
        axios.get( (getUrlBE() + '/curriculum/'+idCV)).then(response => {
        var f = moment(response.data.datos.fecha_nacimiento).format("YYYY-MM-DD") 
        setFecha(f) 
        

        //console.log(f.dateFormat("Y"))
        //setFecha(response.data.datos.fecha_nacimiento)


         setValue("nombres",response.data.datos.nombres)
         setValue("apellidos",response.data.datos.apellidos)
         setValue("direccion",response.data.datos.direccion)
         setValue("genero",response.data.datos.genero)
         
         setValue("correo_electronico",response.data.datos.correo_electronico)
         setValue("dui_pasaporte",response.data.datos.dui_pasaporte)
         setValue("nit",response.data.datos.nit)
         setValue("nup",response.data.datos.nup)


         //document.getElementById("example-datepicker").setValue("2021-02-05")

        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fCurriculum()
  }, [])
  

  const cCurriculum = () => {

    const fecha = document.getElementById("dp-fecha").value

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('nombres', getValues('nombres') );
    bodyFormData.append('apellidos', getValues('apellidos'));
    bodyFormData.append('direccion', getValues('direccion'));
    bodyFormData.append('genero', getValues('genero'));
    bodyFormData.append('fecha_nacimiento',  fecha );
    bodyFormData.append('correo_electronico', getValues('correo_electronico'));
    bodyFormData.append('dui_pasaporte', getValues('dui_pasaporte'));
    bodyFormData.append('nit', getValues('nit'));
    bodyFormData.append('nup', getValues('nup'));
    bodyFormData.append('id', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/curriculumagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro modificado con éxito")
            props.history.push('/curriculumver/'+response.data.datos.id)
        }else{

        }

    }).catch(error => {

    });
    
  }

  
  

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'curriculum', active: true }]}
      className="EmpresaTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Editar curriculum

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblNombres" sm={3}>
                    Nombres
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nombres"
                      {...register("nombres", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblApellidos" sm={3}>
                    Apellidos
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Apellidos"
                      {...register("apellidos", {required: true})}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="lblCorreo" sm={3}>
                    Correo electr&oacute;nico
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="test@test.com"
                      {...register("correo_electronico", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNup" sm={3}>
                    Direcci&oacute;n
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="direccion"
                      {...register("direccion", {required: true})}
                    />
                  </Col>
                </FormGroup>


                <FormGroup row>
                  <Label for="lbl-genero" sm={3}>
                    Genero
                  </Label>
                  <Col sm={9}>
                  <Input type="select" {...register("genero", { required: true })} >
                      <option key={"M"} value={"M"}>{"M"}</option>
                      <option key={"F"} value={"F"}>{"F"}</option>
                  </Input>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lbl-telefono_1" sm={3}>
                    Fecha de nacimiento
                  </Label>
                  <Col sm={9}>
                  
                    <DatePicker 
                    id      = "dp-fecha" 
                    value={fecha.toString()}
                    dateFormat="YYYY-MM-DD"
                    showMonthDropdown
                    showYearDropdown
                    adjustDateOnChange
                   
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lblDui" sm={3}>
                    DUI
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="DUI"
                      {...register("dui_pasaporte", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNit" sm={3}>
                    NIT
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="NIT"
                      {...register("nit", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblNup" sm={3}>
                    NUP
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="NUP"
                      {...register("nup", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/btcurriculums') }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cCurriculum} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtCurriculumEditar;

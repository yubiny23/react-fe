import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtRecpersonalagregar = (props) => {

  const [data, setData] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idCV = props.match.params.id;

  const cRlaboral = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('nombre_contacto', getValues('nombre_contacto') );
    bodyFormData.append('telefono_contacto', getValues('telefono_contacto'));
    bodyFormData.append('telefono_contacto2', getValues('telefono_contacto2'));
    bodyFormData.append('idcv', idCV);

    axios.post( 
      (
        
        getUrlBE() + '/recpersonalagregar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro creado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

    
  

  return (
    <Page
      title="Agregar"
      breadcrumbs={[{ name: 'Recomendación laboral', active: true }]}
      className="CurriculumTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Agregar recomendación laboral

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblContacto" sm={3}>
                    Contácto
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Contácto"
                      {...register("nombre_contacto", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTelefono" sm={3}>
                    Tel&eacute;fono
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tel&eacute;fono"
                      {...register("telefono_contacto", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblTelefono2" sm={3}>
                    Tel&eacute;fono 2
                  </Label>
                  <Col sm={9}>
                  <input
                      type="text"
                      className="form-control"
                      placeholder="Tel&eacute;fono 2"
                      {...register("telefono_contacto2", {required: true})}
                    />
                  </Col>
                </FormGroup>


                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cRlaboral} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtRecpersonalagregar;

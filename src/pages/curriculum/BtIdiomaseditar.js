import Page from 'components/Page';
import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, CardTitle, CardText, Col, Row, Table, Button ,
  Form, FormGroup, Input, Label, FormText,ButtonGroup,ButtonToolbar, Badge
  } from 'reactstrap';
import { Switch, Route, Link } from "react-router-dom";
import moment from "moment";

import {  getUrlBE } from '../../utils/common';
import axios from 'axios';  
import { useForm } from "react-hook-form";

var DatePicker = require("reactstrap-date-picker");


const BtIdiomaseditar = (props) => {


  const [rs, setRs] = useState([])
  const { register, handleSubmit, formState: { errors }, setValue,getValues } = useForm();
  const idC = props.match.params.id;
  const [idCV, setIdcv] = useState([])

   

  useEffect(() => {
  

  const getCharacters = async () => {
      try {

        axios.get( getUrlBE() + '/lenguajeall').then(response => {
            
          setRs( response.data.datos );
            
        }, []).catch(error => console.log(error))


      } catch(err) {
        // error handling code
      } 
  }
  getCharacters();


}, []);

  const cRs = () => {

    //creamos el formulario
    var bodyFormData = new FormData();
    bodyFormData.append('id_lenguaje', getValues("id_lenguaje"))
    bodyFormData.append('conversacion', getValues("conversacion"))
    bodyFormData.append('escritura', getValues("escritura"))
    bodyFormData.append('escucha', getValues("escucha"))
    bodyFormData.append('lectura', getValues("lectura"))
    bodyFormData.append('id', idC);

    axios.post( 
      (
        
        getUrlBE() + '/lenguajecveditar/') ,  bodyFormData 
        

      ).then(response => {

        if ( response.data.estado == "ok" ){
            alert("Registro modificado con éxito")
            props.history.push('/curriculumver/'+idCV)
        }else{

        }

    }).catch(error => {

    });
    
  }

  useEffect(() => {
    const fElaboral = async () => {
      try {
        axios.get( (getUrlBE() + '/lenguajecvid/'+idC)).then(response => {
          
        
          setValue("id_lenguaje",response.data.datos.id_lenguaje)
          setValue("conversacion",response.data.datos.conversacion)
          setValue("escritura",response.data.datos.escritura)
          setValue("escucha",response.data.datos.escucha)
          setValue("lectura",response.data.datos.lectura)
          
          setIdcv(response.data.datos.id_curriculum)
    
        }, []).catch(error => console.log(error))
      } catch(err) {
        // error handling code
      }
    }
    fElaboral()
  });
 

    
  

  return (
    <Page
      title="Editar"
      breadcrumbs={[{ name: 'lenguaje', active: true }]}
      className="redsocialTable"
    >

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Editar lenguaje

              </CardHeader>
              
            <CardBody>
              <Form>
                
              <FormGroup row>
                  <Label for="lblLogro" sm={3}>
                    Lenguaje
                  </Label>
                  <Col sm={9}>

                  <Input type="select" {...register("id_lenguaje", { required: true })} >
                  <option key={0} value={""}>Selecciona una opción</option>
                    {rs.map((r, index) => ( 
                      <option key={r.id_lenguaje} value={r.id_lenguaje}>{r.lenguaje}</option>
                    ))}
                  </Input>
                    
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblConversacion" sm={3}>
                    Conversaci&oacute;n
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Conversaci&oacute;n"
                      {...register("conversacion", {required: true})}
                    />
                  </Col>
                </FormGroup>
                
                <FormGroup row>
                  <Label for="lblEscritura" sm={3}>
                    Escritura
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Escritura"
                      {...register("escritura", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblEscucha" sm={3}>
                    Escucha
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Escucha"
                      {...register("escucha", {required: true})}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="lblCompania" sm={3}>
                    Lectura
                  </Label>
                  <Col sm={9}>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Lectura"
                      {...register("lectura", {required: true})}
                    />
                  </Col>
                </FormGroup>
              
                <ButtonToolbar>
                  <ButtonGroup className="mr-3 mb-3">
                  
                      <Button color="secondary" onClick={() => { props.history.push('/curriculumver/'+idCV) }} >Cancelar</Button>
                  
                  </ButtonGroup>
                  <ButtonGroup className="mr-3 mb-3">
                  
                  <Button color="primary" onClick={cRs} >Guardar</Button>
              
                  </ButtonGroup>
                </ButtonToolbar>
              </Form>
            </CardBody>

            </Card>
          </Col>
        </Row>



    </Page>
  );
};

export default BtIdiomaseditar;

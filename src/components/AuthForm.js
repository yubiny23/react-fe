import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
//import React from 'react';
import React, { useState, useEffect } from 'react';

import { Button, Form, FormGroup, Input, Label } from 'reactstrap';

import {  getUrlBE } from '../utils/common';
import axios from 'axios'; 


class AuthForm extends React.Component {

  

  get isLogin() {
    return this.props.authState === STATE_LOGIN;
  }

  get isSignup() {
    return this.props.authState === STATE_SIGNUP;
  }

  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  

  handleSubmit = event => {

    var bodyFormData = new FormData();
    bodyFormData.append('usuario',document.getElementById("usuario").value );
    bodyFormData.append('password', document.getElementById("password").value );
   
    axios({
      method: 'POST',
      mode: 'cors',
      url: getUrlBE()+'/login',
      data: bodyFormData,
      credentials: 'same-origin',
      headers: { "Content-Type": "multipart/form-data" },
      
    }).then(response => {
            
      if(response.data.code=="200") {
        
        
        alert("login correcto")
        this.props.localStorage.setItem("state", "adasdada");


      }else{
        alert("login correcto");
      } 
      
        
    }, []).catch(error => console.log(error))

    //event.preventDefault();



  };

  renderButtonText() {
    const { buttonText } = this.props;

    if (!buttonText && this.isLogin) {
      return 'Iniciar sesión';
    }

    return buttonText;
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      children,
      onLogoClick,
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        
        <FormGroup>
          <Label for={usernameLabel}>{usernameLabel}</Label>
          <Input  {...usernameInputProps}   />
        </FormGroup>

        <FormGroup>
          <Label for={passwordLabel}>{passwordLabel}</Label>
          <Input   {...passwordInputProps}  />
        </FormGroup>

       

        <hr />
        
        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={this.handleSubmit}>
          {this.renderButtonText()}
        </Button>

        {children}
      </Form>
    );
  }
}

export const STATE_LOGIN = 'LOGIN';
export const STATE_SIGNUP = 'SIGNUP';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'LOGIN',
  showLogo: true,
  usernameLabel: 'Email',
  usernameInputProps: {
    type: 'email',
    placeholder: 'Ingrese su correo',
    name: 'usuario',
    id:'usuario'
  },
  passwordLabel: 'Password',
  passwordInputProps: {
    type: 'password',
    placeholder: 'Ingrese su password',
    name: 'password',
    id:'password'
  },
  confirmPasswordLabel: 'Confirm Password',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirm your password',
  },
  onLogoClick: () => {},
};

export default AuthForm;

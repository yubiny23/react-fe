import React from 'react';

import { Navbar, Nav, NavItem } from 'reactstrap';

import SourceLink from 'components/SourceLink';

const Footer = () => {
  return (
    <Navbar>
      <Nav navbar>
        <NavItem>
          Proyecto de Bolsa de Trabajo 2021
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default Footer;

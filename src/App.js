import { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from 'pages/AuthPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import './styles/reduction.scss';

const AlertPage = React.lazy(() => import('pages/AlertPage'));
const AuthModalPage = React.lazy(() => import('pages/AuthModalPage'));
const BadgePage = React.lazy(() => import('pages/BadgePage'));
const ButtonGroupPage = React.lazy(() => import('pages/ButtonGroupPage'));
const ButtonPage = React.lazy(() => import('pages/ButtonPage'));
const CardPage = React.lazy(() => import('pages/CardPage'));
const ChartPage = React.lazy(() => import('pages/ChartPage'));
const DashboardPage = React.lazy(() => import('pages/DashboardPage'));
const DropdownPage = React.lazy(() => import('pages/DropdownPage'));
const FormPage = React.lazy(() => import('pages/FormPage'));
const InputGroupPage = React.lazy(() => import('pages/InputGroupPage'));
const ModalPage = React.lazy(() => import('pages/ModalPage'));
const ProgressPage = React.lazy(() => import('pages/ProgressPage'));
const TablePage = React.lazy(() => import('pages/TablePage'));
const TypographyPage = React.lazy(() => import('pages/TypographyPage'));
const WidgetPage = React.lazy(() => import('pages/WidgetPage'));

//Seccion de Usuarios administradores
const BtUsuariosa = React.lazy(() => import('pages/admin/BtUsuariosa'));
const BtUsuariosAgregar = React.lazy(() => import('pages/admin/BtUsuariosaAgregar'));
const BtUsuariosEditar = React.lazy(() => import('pages/admin/BtUsuariosaEditar'));

//Seccion de Empresas
const BtEmpresas = React.lazy(() => import('pages/empresas/BtEmpresas'));
const BtEmpresaEditar = React.lazy(() => import('pages/empresas/BtEmpresaEditar'));
const BtEmpresaAgregar = React.lazy(() => import('pages/empresas/BtEmpresaAgregar'));
const BtEmpresaUsuarios = React.lazy(() => import('pages/empresas/BtEmpresasUsuarios'));
const BtEmpresaUsuariosA = React.lazy(() => import('pages/empresas/BtEmpresaUsuarioAgregar'));
const BtEmpresaUsuariosE = React.lazy(() => import('pages/empresas/BtEmpresaUsuarioEditar'));

//Seccion de Curriculum
const BTCurriculums = React.lazy(() => import('pages/curriculum/BtCurriculum'));
const BTCurriculumsVer = React.lazy(() => import('pages/curriculum/BtCurriculumVer'));
const BTCurriculumsAgregar = React.lazy(() => import('pages/curriculum/BtCurriculumAgregar'));
const BTCurriculumsEditar = React.lazy(() => import('pages/curriculum/BtCurriculumEditar'));


const BtCertAgregar = React.lazy(() => import('pages/curriculum/BtCertAgregar'));
const BtCertEditar = React.lazy(() => import('pages/curriculum/BtCertEditar'));
const BtCertEliminar = React.lazy(() => import('pages/curriculum/BtCertEliminar'));


const BtPubAgregar = React.lazy(() => import('pages/curriculum/BtPublicacionAgregar'));
const BtPubEditar = React.lazy(() => import('pages/curriculum/BtPublicacionEditar'));
const BtPubEliminar = React.lazy(() => import('pages/curriculum/BtPublicacionEliminar'));


const BtLogroAgregar = React.lazy(() => import('pages/curriculum/BtLogroagregar'));
const BtLogroEditar = React.lazy(() => import('pages/curriculum/BtLogroeditar'));
const BtLogroEliminar = React.lazy(() => import('pages/curriculum/BtLogroeliminar'));

const BtRSEliminar = React.lazy(() => import('pages/curriculum/BtRseliminar'));
const BtRSAgregar = React.lazy(() => import('pages/curriculum/BtRsagregar'));

const BtCacademicoAgregar = React.lazy(() => import('pages/curriculum/BtCacademicoAgregar'));
const BtCacademicoEditar = React.lazy(() => import('pages/curriculum/BtCacademicoEditar'));
const BtCacademicoEliminar = React.lazy(() => import('pages/curriculum/BtCacademicoEliminar'));

const BtElaboraloAgregar = React.lazy(() => import('pages/curriculum/BtElaboralAgregar'));
const BtElaboraloEditar = React.lazy(() => import('pages/curriculum/BtElaboralEditar'));
const BtElaboralEliminar = React.lazy(() => import('pages/curriculum/BtElaboralEliminar'));

const BtIdiomasAgregar = React.lazy(() => import('pages/curriculum/BtIdiomasagregar'));
const BtIdiomasEditar = React.lazy(() => import('pages/curriculum/BtIdiomaseditar'));
const BtIdiomasEliminar = React.lazy(() => import('pages/curriculum/BtIdiomaseliminar'));

const BtIReclaboralAgregar = React.lazy(() => import('pages/curriculum/BtReclaboralagregar'));
const BtReclaboraleditar = React.lazy(() => import('pages/curriculum/BtReclaboraleditar'));
const BtReclaboraleliminar = React.lazy(() => import('pages/curriculum/BtReclaboraleliminar'));

const BtRecpersonalagregar = React.lazy(() => import('pages/curriculum/BtRecpersonalagregar'));
const BtRecpersonaleditar = React.lazy(() => import('pages/curriculum/BtRecpersonaleditar'));
const BtRecpersonaleliminar = React.lazy(() => import('pages/curriculum/BtRecpersonaleliminar'));


//Seccion de ofertas
const BtOfertaTrabajoa = React.lazy(() => import('pages/ofertas/BtOfertaTrabajoa'));
const BtOfertaTrabajoeditar = React.lazy(() => import('pages/ofertas/BtOfertaTrabajoeditar'));
const BtOfertaTrabajoVer = React.lazy(() => import('pages/ofertas/BtOfertaTrabajoVer'));
const BtOfertaTrabajoAgregar = React.lazy(() => import('pages/ofertas/BtOfertaTrabajoAgregar'));

const BthabdestrezasAgregar = React.lazy(() => import('pages/ofertas/BthabdestrezasAgregar'));
const BthabdestrezasEditar = React.lazy(() => import('pages/ofertas/BthabdestrezasEditar'));
const BthabdestrezasEliminar = React.lazy(() => import('pages/ofertas/BthabdestrezasEliminar'));

//Participaciones en las ofertas
const BtCVParticipacionAgregar = React.lazy(() => import('pages/curriculum/BtCVParticipacionAgregar'));
const BtCVParticipacionEditar = React.lazy(() => import('pages/curriculum/BtCVParticipacionEditar'));
const BtCVParticipacionEliminar = React.lazy(() => import('pages/curriculum/BtCVParticipacionEliminar'));

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  render() {
    return (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          <Switch>
            <LayoutRoute
              exact
              path="/login"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_LOGIN} />
              )}
            />

            
            <MainLayout breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/" component={DashboardPage} />
                <Route exact path="/login-modal" component={AuthModalPage} />
                <Route exact path="/buttons" component={ButtonPage} />
                <Route exact path="/cards" component={CardPage} />
                <Route exact path="/widgets" component={WidgetPage} />
                <Route exact path="/typography" component={TypographyPage} />
                <Route exact path="/alerts" component={AlertPage} />
                <Route exact path="/tables" component={TablePage} />
                <Route exact path="/badges" component={BadgePage} />
                <Route
                  exact
                  path="/button-groups"
                  component={ButtonGroupPage}
                />
                <Route exact path="/dropdowns" component={DropdownPage} />
                <Route exact path="/progress" component={ProgressPage} />
                <Route exact path="/modals" component={ModalPage} />
                <Route exact path="/forms" component={FormPage} />
                <Route exact path="/input-groups" component={InputGroupPage} />
                <Route exact path="/charts" component={ChartPage} />

                <Route exact path="/btusuariosa" component={BtUsuariosa} />
                <Route exact path="/agregarusuario" component={BtUsuariosAgregar} />
                <Route path="/editarusuario/:id" component={BtUsuariosEditar} />

                <Route path="/btempresas" component={BtEmpresas} />
                <Route path="/editarempresa/:id" component={BtEmpresaEditar} />
                <Route path="/agregarempresa" component={BtEmpresaAgregar} />
                <Route path="/usuariosempresa/:id" component={BtEmpresaUsuarios} />
                <Route path="/usuariosempresag/:id" component={BtEmpresaUsuariosA} />
                <Route path="/editarempresau/:id" component={BtEmpresaUsuariosE} />
                

                <Route path="/btcurriculums" component={BTCurriculums} />
                <Route path="/curriculumver/:id" component={BTCurriculumsVer} />
                <Route path="/agregarcurriculum" component={BTCurriculumsAgregar} />
                <Route path="/editarcurriculum/:id" component={BTCurriculumsEditar} />

                <Route path="/agregarcrt/:id" component={BtCertAgregar} />
                <Route path="/editarcrt/:id" component={BtCertEditar} />
                <Route path="/eliminarcrt/:id" component={BtCertEliminar} />


                <Route path="/agregarpublicacion/:id" component={BtPubAgregar} />
                <Route path="/editarpublicacion/:id" component={BtPubEditar} />
                <Route path="/eliminarpublicacion/:id" component={BtPubEliminar} />


                <Route path="/agregarlogro/:id" component={BtLogroAgregar} />
                <Route path="/editarlogro/:id" component={BtLogroEditar} />
                <Route path="/eliminarlogro/:id" component={BtLogroEliminar} />


                <Route path="/eliminarrs/:id" component={BtRSEliminar} />
                <Route path="/agregarrs/:id" component={BtRSAgregar} />

                <Route path="/agregarcademicos/:id" component={BtCacademicoAgregar} />
                <Route path="/editarcademicos/:id" component={BtCacademicoEditar} />
                <Route path="/eliminarcacademicos/:id" component={BtCacademicoEliminar} />

                <Route path="/agregarelaboral/:id" component={BtElaboraloAgregar} />
                <Route path="/editarelaboral/:id" component={BtElaboraloEditar} />
                <Route path="/eliminarelaboral/:id" component={BtElaboralEliminar} />

                <Route path="/agregarlenguaje/:id" component={BtIdiomasAgregar} />
                <Route path="/editarlenguaje/:id" component={BtIdiomasEditar} />
                <Route path="/eliminarlenguaje/:id" component={BtIdiomasEliminar} />
                
                <Route path="/agregarreclaboral/:id" component={BtIReclaboralAgregar} />
                <Route path="/editarreclaboral/:id" component={BtReclaboraleditar} />
                <Route path="/eliminarreclaboral/:id" component={BtReclaboraleliminar} />
                
                <Route path="/agregarrecpersonal/:id" component={BtRecpersonalagregar} />
                <Route path="/editarrecpersonal/:id" component={BtRecpersonaleditar} />
                <Route path="/eliminarrecpersonal/:id" component={BtRecpersonaleliminar} />
                

                <Route path="/ofertas" component={BtOfertaTrabajoa} />
                <Route path="/ofertaeditar/:id" component={BtOfertaTrabajoeditar} />
                <Route path="/ofertaver/:id" component={BtOfertaTrabajoVer} />
                <Route path="/ofertaagregar" component={BtOfertaTrabajoAgregar} />

                <Route path="/habdestrezas/:id" component={BthabdestrezasAgregar} />
                <Route path="/editarhabdestrezas/:id" component={BthabdestrezasEditar} />
                <Route path="/eliminarhabdetrezas/:id" component={BthabdestrezasEliminar} />


                <Route path="/agregarparticipacion/:id" component={BtCVParticipacionAgregar} />
                <Route path="/editarparticipacion/:id" component={BtCVParticipacionEditar} />
                <Route path="/eliminarparticipacion/:id" component={BtCVParticipacionEliminar} />
                
                                
                
              </React.Suspense>
            </MainLayout>
            <Redirect to="/" />
          </Switch>
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
